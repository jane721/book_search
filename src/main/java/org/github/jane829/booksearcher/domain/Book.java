package org.github.jane829.booksearcher.domain;

import java.util.Date;

/**
 * Created by jyyu on 8/24/15.
 */
public class Book {
    private String title;
    private String author;
    private Date published;


    public Book(String title, String author, Date published) {
        this.title = title;
        this.author = author;
        this.published = published;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public Book withTitle(String title){
        setTitle(title);
        return this;
    }

    public Book withAuthor(String author){
        setAuthor(author);
        return this;
    }


    public Book withPublished(Date date){
        setPublished(date);
        return this;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Date getPublished() {
        return published;
    }

    public void setPublished(Date published) {
        this.published = published;
    }
}
