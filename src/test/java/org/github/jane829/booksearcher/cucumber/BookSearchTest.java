package org.github.jane829.booksearcher.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "html:../build/cucumber", "json:../build/cucumber/cucumber.json"},
        tags = {"@tag"},
        features = {"src/test/resources"}
)
public class BookSearchTest {

}
