package org.github.jane829.booksearcher.cucumber;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.github.jane829.booksearcher.domain.Book;
import org.github.jane829.booksearcher.domain.Library;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

/**
 * Created by jyyu on 8/24/15.
 */
public class BookSearchSteps {
    private List<Book> results = new ArrayList<Book>();
    private Library library = new Library();

    @Given("^a book with the title 'One good book', written by 'Anonymous', published in (\\d+) March (\\d+)$")
    public void a_book_with_the_title_One_good_book_written_by_Anonymous_published_in_March(int arg1, int arg2) throws Throwable {
        Book book = new Book("One good book", "Anonymous", new Date(arg2, 3, arg1));
        library.addBook(book);
    }

    @Given("^another book with title 'Some other book', written by 'Tim Tomson',published in (\\d+) August (\\d+)$")
    public void another_book_with_title_Some_other_book_written_by_Tim_Tomson_published_in_August(int arg1, int arg2) throws Throwable {
        Book book = new Book("Some other book", "Tim Tomson", new Date(arg2, 8, arg1));
        library.addBook(book);
    }

    @Given("^another book with title 'How to cook  dino', written by ‘Fred Flinstone’, published in (\\d+) January (\\d+)$")
    public void another_book_with_title_How_to_cook_dino_written_by_Fred_Flinstone_published_in_January(int arg1, int arg2) throws Throwable {
        Book book = new Book("How to cook  dino", "Fred Flinstone", new Date(arg2, 1, arg1));
        library.addBook(book);
    }

    @When("^the customer searches for books published between (\\d+) and (\\d+)$")
    public void the_customer_searches_for_books_published_between_and(int arg1, int arg2) throws Throwable {
        results = library.findBooks(new Date(arg1, 1, 1), new Date(arg2, 12, 31));
    }

    @Then("^(\\d+) books should have been found$")
    public void books_should_have_been_found(int arg1) throws Throwable {
        assertThat(arg1, equalTo(results.size()));
    }

    @Then("^Book (\\d+) should have the title 'Some other book'$")
    public void book_should_have_the_title_Some_other_book(int arg1) throws Throwable {
        assertThat(results.get(0).getTitle(), is("Some other book"));
    }

    @Then("^Book (\\d+) should have the title 'One good book'$")
    public void book_should_have_the_title_One_good_book(int arg1) throws Throwable {
        assertThat(results.get(1).getTitle(), is("One good book"));
    }
}
